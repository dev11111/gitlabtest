package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Demo222Controller {
    @GetMapping("/")
    public  String method1(){return "method1";}

    @GetMapping("/second")
    public  String method2(){return "method2";}

    @GetMapping("/gitlabtest")
    public  String method3test(){return "method3";}


}
